package com.flycms.web.admin;

import com.flycms.core.base.BaseController;
import com.flycms.core.entity.DataVo;
import com.flycms.module.search.service.SolrService;
import com.flycms.module.share.model.Share;
import com.flycms.module.share.service.ShareService;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 12:05 2018/9/3
 */
@Controller
@RequestMapping("/admin/share")
public class ShareAdminController extends BaseController {
    @Autowired
    private ShareService shareService;

    @Autowired
    private SolrService solrService;

    @GetMapping(value = "/list_share")
    public String shareList(@RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap){
        modelMap.addAttribute("p", p);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("content/list_share");
    }

    @ResponseBody
    @RequestMapping(value = "/index_all_share")
    public DataVo indexAllShare() {
        DataVo data = DataVo.failure("操作失败");
        try {
            solrService.indexAllShare();
            data=DataVo.success("全部索引成功！");
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //文章列表
    @GetMapping(value = "/category_list")
    public String getShareList(@RequestParam(value = "title", required = false) String title,
                                  @RequestParam(value = "createTime", required = false) String createTime,
                                  @RequestParam(value = "p", defaultValue = "1") int pageNum,
                                  ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("content/list_share_category");
    }

    //查询单条问题数据信息
    @ResponseBody
    @GetMapping(value = "/findId")
    public DataVo getFindShareId(@RequestParam(value = "id", required = false) String id, ModelMap modelMap) {
        DataVo data = DataVo.failure("操作失败");
        try {
            if (!NumberUtils.isNumber(id)) {
                return data = DataVo.failure("id参数错误");
            }
            Share question=shareService.findShareById(Integer.valueOf(id),0);
            if(question==null) {
                return DataVo.failure("id错误或不存在！");
            }else {
                return DataVo.success("查询成功", question);
            }
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //问题审核操作
    @PostMapping("/share-status")
    @ResponseBody
    public DataVo updateShareStatusById(@RequestParam(value = "id", required = false) String id,
                                           @RequestParam(value = "status", required = false) String status,
                                           @RequestParam(value = "recommend", defaultValue = "0") String recommend) throws Exception{
        DataVo data = DataVo.failure("操作失败");
        if (!NumberUtils.isNumber(id)) {
            return data = DataVo.failure("id参数错误");
        }
        if (!NumberUtils.isNumber(status)) {
            return data = DataVo.failure("审核状态参数错误");
        }
        if (!StringUtils.isBlank(recommend)) {
            if (!NumberUtils.isNumber(recommend)) {
                return data = DataVo.failure("推荐参数错误");
            }
        }
        data = shareService.updateShareStatusById(Integer.valueOf(id),Integer.valueOf(status),Integer.valueOf(recommend));
        return data;
    }
}
